package de.fh_kiel.iue.mob;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import de.fh_kiel.android.exampleactivity.R;

public class MainActivity extends AppCompatActivity {

    static final int M_REQUEST_CODE = 42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, M_REQUEST_CODE);
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == M_REQUEST_CODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

                // The user picked a contact.
                Uri contactUri = data.getData();
                // We only need the DISPLAY_NAME column
                String[] projection = {ContactsContract.Data.DISPLAY_NAME};

                // Perform the query on the contact to get the DISPLAY_NAME column
                // We don't need a selection or sort order (there's only one result for the given URI)
                // CAUTION: The query() method should be called from a separate thread to avoid blocking
                // your app's UI thread. (For simplicity of the sample, this code doesn't do that.)
                // Consider using CursorLoader to perform the query.
                Cursor cursor = getContentResolver().query(contactUri, projection, null, null, null);
                cursor.moveToFirst();

                // Retrieve the phone number from the NUMBER column
                int column = cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME);
                String name = cursor.getString(column);

                cursor.close();

                Toast toast = Toast.makeText(getApplicationContext(), "You chose: " + name, Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }


    public void displaytoast(View v) {

        toastMsg("Luca Znaniewicz 922127 Julius Dietrich 932906 Tom Calvin 933010");

    }

    public void toastMsg(String msg) {

        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
        toast.show();

    }

    public void uebergabe (View view){

        EditText input = findViewById(R.id.InputData);
        String valInput = input.getText().toString();

        Intent intent = new Intent(this, ActivityLabel.class);


        intent.putExtra("ValInput", valInput);
        startActivity(intent);

    }
}
