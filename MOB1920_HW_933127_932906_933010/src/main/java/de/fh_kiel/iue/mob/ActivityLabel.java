package de.fh_kiel.iue.mob;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import de.fh_kiel.android.exampleactivity.R;

public class ActivityLabel extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_label);
        Intent intent = getIntent();
        TextView output = findViewById(R.id.TextData);
        output.setText(intent.getStringExtra("ValInput"));
    }
}
