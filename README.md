ExampleActivity
===============

Description
-----------
This example demonstrates the call of an Activity for a specific result
using the function `startActivityForResult`.

As a side effect it also demonstrates using a `Cursor` for extraction of data from
a `ContentResolver`. See callback function `onActivityResult`.

Environment
------------
This example was written and tested with Android Studio 3.1